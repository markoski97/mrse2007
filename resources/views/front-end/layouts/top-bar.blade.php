<!-- top bar -->
<div class="topbar-full">

    <div class="container topbar-content">

        <div class="col-md-8">

            <ul>
                <li><i class="icon-phone-1"></i> +389 71 230 015</li>
                <li><i class="icon-location"></i>3ти Ноември број 12, Прилеп 7500</li>
                <li><i class="icon-clock-1"></i> Понеделник - Сабота: 08:00 - 16:00</li>
            </ul>

        </div>
        <div class="col-md-4 right">

            <ul class="social-top right">
                <li class="nopad"><a href="#" class="facebook-alt" title="Facebook" data-rel="tooltip" data-placement="top"><i class="icon-facebook"></i></a>
                </li>
            </ul>

        </div>
    </div>
</div>
<!-- / top bar -->
