<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/app', 'Front\FrontController@app');
Route::get('/', 'Front\FrontController@home');

Route::post('/produktii', 'Front\ProduktiController@image_store')->name('image_store');
Route::delete('/produktii_delete/{post}', 'Front\ProduktiController@image_delete')->name('image_delete');

//produkti
Route::resource('produkti', 'Front\ProduktiController');
Route::get('/', 'Front\FrontController@home');

//kontakt
Route::get('/kontakt', 'Front\FrontController@kontakt');
Route::post('/mail', 'Front\FrontController@mail')->name('mail');




