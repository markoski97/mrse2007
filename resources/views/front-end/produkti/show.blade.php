@extends('front-end.app')

@section('content')
    <!-- inner banner -->
    <section class="bg-white inner-banner " style="background: url({{asset('uploads/produkti/'.$produkti->image)}})">

        <div class="container pad-container">
            <div class="col-md-12">
                <h1>Project Details</h1>
                <h3>Showcasing what we do</h3>
            </div>
        </div>

    </section>
    <!-- / inner banner -->
    <!-- skills section -->
    <section class="bg-gray">

        <div class="container container-pad">

            <div class="col-sm-12">
                <h2>{{$produkti->name}}</h2>
                <p>{{$produkti->description}} </p>

            </div>
        </div>

    </section>
    <!--/ skills section -->
    <section>

        <div class="container pad-container2">
            <!-- projects grid -->
            <ul class="projects-grid-3 project-grid-gut hide-titles" id="project-grid">
            @foreach($image as $images)
                <!-- project Item -->
                    <li class="project-item mix branding">
                        <a href="{{asset('uploads/post/'.$images->image)}}" class="fancybox" data-rel="portfolio"
                           data-title-id="img-02">
                            <div class="project-img">
                                <img class="project-img" src="{{asset('uploads/post/'.$images->image)}}" alt="project"
                                     style="min-height: 200px;max-height:201px;"/>
                            </div>
                            @auth
                                <div class="project-intro">
                                    <div class="col-sm-12">
                                        <form
                                            id="delete-form-{{$images->id}}"
                                            action="{{route('image_delete',$images->id)}}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                        <button type="button" class="btn btn-danger btn-sm col-sm-6"
                                                onclick="if(confirm('Дали си Сигурен'))
                                                    {
                                                    event.preventDefault();
                                                    document.getElementById('delete-form-{{$images->id}}').submit();
                                                    }
                                                    ">
                                            Избриши
                                        </button>
                                    </div>
                                </div>
                        </a>
                        @endauth
                    </li>
                    <!-- End project Item -->
                @endforeach
            </ul>
            <!-- / projects grid -->
            <div class="col-md-12">
                <div class="col-md-5 ">
                    <div class="well col-md-5-offset-5">
                        <dl class="dl-horizontal">

                            <dt>Креирано на:</dt>
                            <dd>{{date('M j, Y H:i',strtotime($produkti->created_at))}}</dd>
                        </dl>

                        <dl class="dl-horizontal">
                            <dt>Последна промена на:</dt>
                            <dd>{{date('M j, Y H:i',strtotime($produkti->updated_at))}}</dd>
                        </dl>
                        <hr>

                        <div class="row">

                            <div class="col-sm-12">
                                <form
                                    id="delete-form-{{$produkti->id}}"
                                    action="{{route('produkti.destroy',$produkti->id)}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                </form>
                                <button type="button" class="btn btn-danger btn-sm col-sm-6"
                                        onclick="if(confirm('Дали си Сигурен'))
                                            {
                                            event.preventDefault();
                                            document.getElementById('delete-form-{{$produkti->id}}').submit();
                                            }
                                            ">
                                    Избриши
                                </button>
                                <div class="col-sm-6">
                                    <a class="btn btn-danger btn-sm col-sm-12" href="{{route("produkti.create")}}"
                                    >Додај Слики </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </section>
    <!--/ portfolio section -->
    <section>
        <aside>

        </aside>
    </section>

@endsection
