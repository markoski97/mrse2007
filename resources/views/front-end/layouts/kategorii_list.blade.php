<!-- portfolio carousel section -->
<section class="bg-gray">

    <div class="container pad-container">

        <div class="row col-md-12 text-center">
            <h2 class="heading colored">Производи</h2>
            {{-- <p class="h-sub">Овде да се листа најновите категории</p>--}}
        </div>

        <div class="row">

            <div class="col-md-12  t-mgr40">

                <ul class="owl-carousel owlcarousel-full projects-grid hide-titles">
                    @foreach($produkti as $produktis)
                        <li class="item project-item mix branding">

                            <a href="/produkti/{{$produktis->slug}}">
                                <div class="project-img">
                                    <img class="project-img" src="{{asset('uploads/produkti/'.$produktis->image)}}" alt="project" style="max-height: 240px;min-height: 239px"/>
                                </div>
                                <div class="project-intro">
                                    <h3 class="project-title">{{$produktis->name}}</h3>
                                </div>
                            </a>

                        </li>
                    @endforeach
                </ul>

            </div>
        </div>
    </div>
</section>
<!--/ portfolio carousel section -->
