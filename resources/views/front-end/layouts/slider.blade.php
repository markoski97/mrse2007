<!-- revolution slider -->
<div class="banner-container revolution nomgr">
    <div class="banner">
        <ul>

            <li data-transition="fade"><img src="{{asset('front-end/images/slider/ograda.jpg')}}" alt=""/>

                <div class="caption sfb upper2" data-x="20" data-y="200" data-speed="300" data-start="800"
                     data-easing="Sine.easeOut">
                    {{--    <h3>Овде текст да се одбери</h3>--}}
                </div>

                <div class="caption sfb upper2" data-x="10" data-y="240" data-speed="300" data-start="1000"
                     data-easing="Sine.easeOut">
                    <h2>Мрсе2007</h2>
                </div>

                <div class="caption sfb upper2" data-x="20" data-y="360" data-speed="300" data-start="1200"
                     data-easing="Sine.easeOut">
                    <h3 style="color:yellow"><span>Браварска работилница</span ></h3>
                </div>

                <div class="caption sfb" data-x="20" data-y="444" data-speed="300" data-start="1400"
                     data-easing="Sine.easeOut">
                    <a class="btn" href="{{url("/produkti")}}" target="_self">Производи</a>
                </div>
            </li>

            <li data-transition="fade"><img src="{{asset('front-end/images/slider/terasa.jpg')}}" alt=""/>

                <div class="caption sfb upper2" data-x="10" data-y="240" data-speed="300" data-start="1000"
                     data-easing="Sine.easeOut">
                    <h2 style="font-size:80px">Изработка на </h2>
                </div>

                <div class="caption sfb upper2" data-x="20" data-y="360" data-speed="300" data-start="1200"
                     data-easing="Sine.easeOut">
                    <h3 style="color:yellow"><span>Алуминиумски, Ковани огради и порти</span ></h3>
                </div>
            </li>

            <li data-transition="fade"><img src="{{asset('front-end/images/slider/bager.jpg')}}" alt=""/>

                <div class="caption sfb upper2" data-x="10" data-y="240" data-speed="300" data-start="1000"
                     data-easing="Sine.easeOut">
                    <h2 style="font-size:80px">Изработка на </h2>
                </div>

                <div class="caption sfb upper2" data-x="20" data-y="360" data-speed="300" data-start="1200"
                     data-easing="Sine.easeOut">
                    <h3 style="color:yellow"><span>Челични конструкции и Метални настрешници</span ></h3>
                </div>

            </li>
        </ul>
        <div class="tp-bannertimer tp-bottom"></div>
    </div>
    <!-- /.banner -->
</div>
