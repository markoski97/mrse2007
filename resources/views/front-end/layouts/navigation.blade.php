<!-- logo bar -->
<div class="container header-nav pad2">

    <div class="col-sm-3 col-xs-12">
        <div class="basic-wrapper">
            <a class="mobile-menu right" data-toggle="collapse" data-target=".navbar-collapse"><i class='icon-menu'></i></a>
            <a class="logo pad" href="{{url('/')}}"><img src="{{asset('front-end/images/logo/mrsenew.jpg')}}"  alt=""/>
            </a>
        </div>
    </div>

    <div class="col-sm-9">
        <div class="collapse navbar-collapse right">

            <!-- navigation -->
            <ul class="nav navbar-nav hom2">
                <li class="dropdown pad {{Request::is('/')? 'current': ''}} "><a href="{{url('/')}}" class="dark">Почетна</a>
                </li>
                <li class="dropdown pad {{Request::is('produkti') ? 'current': '' }} "><a href="{{url('/produkti')}}" class="dark">Продукти</a>
                </li>
                <li class="dropdown pad {{Request::is('kontakt')? 'current': '' }}"><a href="{{url('kontakt')}}" class="dark">Контакт</a>
                </li>
                {{--<li class="dropdown pad"><a href="about.html" class="dark">About Us</a>
                </li>
                <li class="dropdown pad"><a href="news.html" class="dark">News</a>
                </li>
                <li class="dropdown pad"><a href="shop.html" class="dark">Shop</a>
                </li>
                <li class="dropdown pad"><a href="contact.html" class="dark">Contact</a>
                </li>--}}

            </ul>
            <!-- /navigation -->

        </div>
    </div>

</div>
<!--/ logo bar -->
