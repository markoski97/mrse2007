<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Mail\ContactMail;
use App\Produkti;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class FrontController extends Controller
{
    public function app(){
        return view('front-end.app');
    }

    public function home(){

        $produkti=Produkti::all();
        $bla=Produkti::all();
        return view('front-end.home',compact('produkti','bla'));
    }

    public function kontakt(){
        return view('front-end.kontakt.index');
    }

    public function mail(Request $request){
        Mail::to('petargigant@live.com')->send(new ContactMail($request));
        return redirect()->back();
    }
}
