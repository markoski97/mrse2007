@extends('front-end.app')

@section('content')
    @include('front-end.layouts.slider')
    <!-- /.fullwidthbanner-container -->


    <!-- services section -->
    <section class="bg-white">

        <div class="row">
            <div class="col-md-4 box-dark color1" style="height: 280px">
                <div class="services-box-3">
                    <i class="icon-picons-configuration"></i>
                    <div class="content">
                        <h3>Кратка биографија</h3>
                        <p>Мрсе 2007 е фамилијарно основана фирма во Прилеп. </p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 box-dark" style="height: 280px">
                <div class="services-box-3">
                    <i class="icon-picons-ruler"></i>
                    <div class="content">

                        <h3>Производи</h3>
                        <p>Изработуваме алуминиумски и ковани огради и порти, метални настрешници,
                            челични конструции, изработка на панели на CNC и изработка на интериер од метал.</p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 box-dark color2" style="height: 280px">
                <div class="services-box-3">
                    <i class="icon-picons-pencil-ruler"></i>
                    <div class="content">
                        <h3>Проширеност</h3>
                        <p>Изведуваме, доставуваме и монтираме низ цела Македонија.</p>
                    </div>
                </div>
            </div>

        </div>

    </section>
    <!--/ services section -->


    <!-- features section -->
    <section>

        <div class="container pad-container2">

            <div class="row">

                <div class="col-md-4">
                    <div class="services-box-2">
                        <i class="icon-picons-plane"></i>
                        <div class="content">
                            <h3>Алуминиумски огради</h3>
                            <p>Се изработуваат од високо квалитетни, елуксираниалуминиумски профили и акцесоари.</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="services-box-2">
                        <i class="icon-picons-building"></i>
                        <div class="content">
                            <h3>Метални огради и порти</h3>
                            <p>Изработка на сите видови на метални огради и порти : ковани, ролента, тараба и огради и
                                порти по желба на купувачот.</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="services-box-2">
                        <i class="icon-picons-flower"></i>
                        <div class="content">
                            <h3>Настрешници</h3>
                            <p>Изработка на сите видови настрешници со лексан, лим, панел и комбнација алуминиум и метал.</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row t-mgr50">

                <div class="col-md-4">
                    <div class="services-box-2">
                        <i class="icon-picons-configuration-2"></i>
                        <div class="content">
                            <h3>Челични конструкции</h3>
                            <p>Изработка и монтажа на челични конструкции од најмала гаража до производствена хала.</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="services-box-2">
                        <i class="icon-picons-hammer"></i>
                        <div class="content">
                            <h3>ЦНЦ </h3>
                            <p>Изработка на метални панели со најразлични фигури и фомри со дебелина на лим од 2-35мм.</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="services-box-2">
                        <i class="icon-picons-hammer"></i>
                        <div class="content">
                            <h3>Други услуги </h3>
                            <p>Изнајмуваме електрична платформа (подвижно скеле),изработка на интерирер од метал и
                                секакви метални продукти и продажба на аксесоари за алуминиумски огради и оков за PVC дограма. </p>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </section>
    <!-- features section -->


  @include('front-end.layouts.kategorii_list')


@include('front-end.layouts.pozadina')

    <!--/ parallax section -->


    @include('front-end.layouts.contact_bar')

@endsection
