<head>
    <!-- Basic Page Needs
================================================== -->
    <meta charset="utf-8">
    <title>Gates - Multi-Purpose Construction Template</title>
    <!-- Favicon
    ============================================== -->
    <link rel="shortcut icon" href="{{asset('front-end/images/logo/fav.png')}}">
    <!-- Mobile Specific
================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- CSS Files
================================================== -->
    <link rel="stylesheet" type="text/css" href="{{asset('front-end/css/bootstrap.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('front-end/css/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('front-end/css/responsive.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('front-end/css/color/yellow.css')}}" id="colors">
    <link rel="stylesheet" type="text/css" href="{{asset('front-end/css/settings.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('front-end/css/owl.carousel.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('front-end/css/jquery.fancybox.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800'>
    <link rel='stylesheet' type='text/css' href='https://fonts.googleapis.com/css?family=Montserrat:100,300,400,600,700,800,900'>
    <link rel="stylesheet" type="text/css" href="{{asset('front-end/css/fonts/fontello.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('front-end/css/fonts/gizmo.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('front-end/css/fonts/picons.css')}}">

    <!-- Modernizr js file
================================================== -->
    <script src="{{asset('front-end/js/modernizr.custom.js')}}"></script>
</head>
