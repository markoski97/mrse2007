<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Post;
use App\Produkti;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class ProduktiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'show']]);
    }

    public function index()
    {
        $produkti = Produkti::all();
        return view('front-end.produkti.index', compact('produkti'));
    }

    public function create()
    {
        $produkti = Produkti::all();
        return view('front-end.produkti.create',compact('produkti'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
            'image' => 'image|mimes:jpg,png,jpeg,gif',
        ],
            [
                'title.required' => 'Немате внесено име',
                'description.required' => 'Немате внесено опис',
                'image.required' => 'Немате внесено слика'
            ]);

        $image = $request->file('image');

        $slug = Str::slug($request->name);

        if (isset($image)) {
            $imagename = $slug . '.' . uniqid() . '.' . $image->getClientOriginalExtension();
            if (!file_exists('uploads/produkti')) {
                mkdir('uploads/produkti', 0777, true);
            }
            $image->move('uploads/produkti', $imagename);
        } else {
            $imagename = 'default.png';
        }


        $produkti = new Produkti();
        $produkti->name = $request->name;
        $produkti->description = $request->description;
        $produkti->image = $imagename;
        $produkti->slug = $slug;

        $produkti->save();

        Session::flash('success', 'Успешно внесување');
        return redirect()->back();
    }

    public function show($slug)
    {
        $produkti = Produkti::where('slug', '=', $slug)->first();
        $image=Post::where('product_id','=',$produkti->id)->get();

        return view('front-end.produkti.show', compact('produkti','image'));
    }

    public function edit($id)
    {
        $produkti = Produkti::findorfail($id);

        return view('front-end.produkti.edit', compact('produkti'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
        ],
            [
                'title.required' => 'Немате внесено име',
                'description.required' => 'Немате внесено опис',
            ]);

        $produkti = Produkti::findorfail($id);
        $slug = Str::slug($request->name);

        $image = $request->file('image');
        if (isset($image)) {
            $currentDate = Carbon::now()->toDateString();
            $imageName = $slug . '-' . $currentDate . '-' . uniqid() . '-' . $image->getClientOriginalName();

            if (!file_exists('uploads/produkti')) {
                mkdir('uploads/produkti', 0777, true);
            }
            $image->move('uploads/produkti', $imageName);
        } else {
            $imageName = $produkti->image;//Ako ne klaj nova slika Zemija starata
        }
        $produkti->name = $request->name;
        $produkti->description = $request->description;
        $produkti->image = $imageName;
        $produkti->slug = $slug;
        $produkti->save();

        Session::flash('success', 'Успешна Промена');
        return redirect()->back();
    }

    public function destroy($id)
    {
        $produkti = Produkti::findorfail($id);
        if (file_exists('uploads/produkti/' . $produkti->image)) {
            unlink('uploads/produkti/' . $produkti->image);
        }
        $produkti->delete();
        return redirect()->route('produkti.index');
    }

    public function image_store(Request $request)
    {
        foreach (request()->file('image') as $file) {
            $imagealbum = new Post();
            $filename = $file->getClientOriginalName();

            if (!file_exists('uploads/post')) {
                mkdir('uploads/post', 0777, true);
            }
            $file->move('uploads/post', $filename);
            $imagealbum->product_id = $request->post;
            $imagealbum->image = $filename;

            $imagealbum->save();
        }
        return redirect()->route('produkti.index');
    }

    public function image_delete($id){
        $imagealbum=Post::findorfail($id);
        if (file_exists('uploads/post/' . $imagealbum->image)) {
            unlink('uploads/post/' . $imagealbum->image);
        }
        $imagealbum->delete();
        return redirect()->back();
    }
}
