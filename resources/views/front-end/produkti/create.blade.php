@extends('front-end.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h1>Креирај Нов Продукт</h1>
                <hr>
                <form method="POST" enctype="multipart/form-data" action="{{ route('produkti.store') }}">
                    @csrf

                    <div class="form-group">
                        <label name="title">Наслов:</label>
                        <input id="title" type="text" name="name"
                               class="form-control{{$errors->has('name') ? 'is-invalid' : ''}}"
                               value="{{ old('name') }}"
                               autocomplete="name" autofocus required>
                        @if($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{$errors->first('name') }}</strong>
                            </span>

                        @endif
                    </div>

                    <div class="form-group">
                        <label name="title">Дополнителни инфромации:</label>
                        <textarea id="description" type="text" name="description"
                                  class="form-control{{$errors->has('description') ? 'is-invalid' : ''}}"
                                  value="{{ old('description') }}"
                                  autocomplete="description" autofocus required> </textarea>
                        @if($errors->has('description'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{$errors->first('description') }}</strong>
                            </span>

                        @endif
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <label for="image" class="col-md-4 col-form-label">Слика:</label>
                            <input type="file" class="form-control-file" id="image" name="image" required>
                            @if($errors->has('image'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{$errors->first('image') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <br>
                    <input type="submit" value="Креирај Категорија" class="btn btn-success btn-lg btn-block">
                    <br>
                    <br>
                    <br>
                </form>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h1>Креирај Слики за Продукт</h1>
                <hr>
                <form method="POST" action="{{route('image_store')}}" enctype="multipart/form-data">
                    @csrf

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="bmd-label-floating">Пост:</label>
                                <select class="form-control " name="post">
                                    <option disabled selected>Одбери</option>
                                    @foreach($produkti as $produktis)
                                        <option value="{{$produktis->id}}">{{$produktis->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <label for="image" class="col-md-4 col-form-label">Слики:</label>
                            <input type="file" class="form-control-file" id="image" name="image[]" multiple>
                            @if($errors->has('image'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{$errors->first('image') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
<br>

                    <input type="submit" value="Креирај Слики" class="btn btn-success btn-lg btn-block">

                </form>

            </div>
        </div>
    </div>




@endsection
