@extends('front-end.app')

@section('content')

    <!-- inner banner -->
    <section class="bg-white inner-banner pages">

        <div class="container pad-container">
            <div class="col-md-12">
                <h1>Контакт</h1>

            </div>
        </div>

    </section>
    <!-- / inner banner -->
    <!-- Ovde mal komentar klavame za proba na git -->

    <!-- intro section -->
    <section class="bg-white">
        <div class="container pad-container">

            <div class="row col-md-12 text-center">
                <h2 class="heading">Контакт</h2>
                <p>Почитувани корисници, за било какви прашања и информации ве молиме посетете не или контактирајте не:</p>
            </div>

            <div class="clearfix"></div>

            <div class="row t-pad40">

                <div class="col-md-6">

                    <div class="form-container">
                        <div class="response alert alert-success"></div>
                        <form action="{{route('mail')}}" method="POST" id="contact-form" class="padding-onlytop-md padding-md-topbottom-null" >
                            {{csrf_field()}}

                            <div class="row b-mgr20">

                                <div class="col-sm-6">
                                    <input class="form-field" name="name" type="text" value="" placeholder="Име" />
                                </div>

                                <div class="col-sm-6">
                                    <input class="form-field" name="phone" type="text" value="" placeholder="Телефон" />
                                </div>

                            </div>
                            <div class="row b-mgr20">
                                <div class="col-sm-6">
                                    <input class="form-field" name="email" type="text" value="" placeholder="Е-Маил" />
                                </div>
                                <div class="col-sm-6">
                                    <input class="form-field" name="subject" type="text" value="" placeholder="Прашање" />
                                </div>
                            </div>
                            <div class="row b-mgr20">
                                <div class="col-sm-12">
                                    <textarea class="form-field" name="message" cols="40" rows="3" placeholder="Вашата Порака"></textarea>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <input type="submit" class="btn-submit" value="Испрати" />
                                </div>
                            </div>
                        </form>
                    </div>

                </div>

                <div class="col-md-6">

                    <div class="row l-pad40">

                        <div class="services-box-2">
                            <i class="icon-picons-pin-2"></i>
                            <div class="content">
                                <p>Suite No 543, Bishop Street,
                                    <br>United Kingdom (UK) 49530.</p>
                            </div>
                        </div>

                        <div class="services-box-2 t-pad20">
                            <i class="icon-picons-telephone-call"></i>
                            <div class="content">
                                <p>+123 456 7890
                                    <br> +020 336 8890</p>
                            </div>
                        </div>

                        <div class="services-box-2 t-pad20">
                            <i class="icon-picons-envelope"></i>
                            <div class="content">
                                <p><a href="mailto:contact@gatescompany.com">contact@gatescompany.com</a>
                                    <br>
                                    <a href="mailto:sales@gatescompany.com">sales@gatescompany.com</a>
                                </p>
                            </div>
                        </div>

                    </div>

                    <div class="row t-mgr30">

                        <div class="map-bg">



                        </div>

                    </div>

                </div>

            </div>

        </div>
    </section>
    <!--/ intro section -->

@endsection
