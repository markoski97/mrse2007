<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produkti extends Model
{
    protected $table = 'produkti';

    public function images(){
        return $this->hasMany('App\Post');
    }

}
