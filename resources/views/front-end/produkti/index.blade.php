@extends('front-end.app')

@section('content')
    <!-- portfolio section -->
    <section>
        @auth()
            <div class="col-md-2 offset-10">
                <a href="{{route("produkti.create")}}" class="btn btn-info">Креирај нова Категорија</a>
            </div>
        @endauth
        <div class="container pad-container2">

            <!-- projects grid -->

            <ul class="projects-grid-3 project-grid-gut hide-titles" id="project-grid">
                <!-- project item -->
                @foreach($produkti as $produktis)
                    <li class="project-item mix branding">
                        <a href="{{url('produkti/'.$produktis->slug)}}">
                            <div class="project-img">
                                <img class="project-img" src="{{asset('uploads/produkti/'.$produktis->image)}}" style="max-height: 240px"
                                     alt="project"/>
                            </div>
                            <div class="project-intro">
                                <h3 class="project-title">{{$produktis->name}}</h3>
                            </div>
                        </a>
                    </li>
                  @endforeach
            <!-- / project item -->
            </ul>
            <!-- / projects grid -->
        </div>


    </section>

@endsection
