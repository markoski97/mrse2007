<!-- footer -->
<footer class="footer">
    <div class="container pad-container">

        <div class="row">
            <div class="col-sm-4 ">
                <div class="widget r-pad50">
                    <a href="{{url('/login')}}"><img src="{{asset('front-end/images/logo/mrsefooter.jpg')}}" style="width: 250px" alt=""/></a>
                    <p>Мрсе 2007 е фамилијарно основана фирма во Прилеп.</p>
                    <div class="clearfix"></div>
                    <div class="social-icons text-center">
                        <ul>

                            <li><a href="#" class="facebook-alt text-center" title="Facebook" data-rel="tooltip"
                                   data-placement="top"><i class="icon-facebook"></i></a>
                            </li>
                            <li><a href="#" class="google-alt" title="Google Plus" data-rel="tooltip"
                                   data-placement="top"><i class="icon-gplus"></i></a>
                            </li>


                        </ul>
                    </div>

                </div>
            </div>
            <div class="col-sm-4">
                <div class="widget">
                    <h4>Продукти</h4>

                    <div class="col-sm-6 col-xs-12">
                        <ul class="circled">
                            @{{--foreach($bla as $blaa)
                            <li>  <a href="/produkti/{{$blaa->slug}}">{{$blaa->name}}</a></li>
                            @endforeach--}}
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 ">
                <div class="widget">
                    <h4>Контакт</h4>
                    <div class="contact">
                        <i class="icon-picons-pin-2"></i>
                        <div class="content">
                            <p class="bold  t-pad30">улица "3 Ноември" број 12, <br>Прилеп, 7500
                            </p>
                        </div>
                    </div>
                    <div class="contact">
                        <i class="icon-picons-telephone-call"></i>
                        <div class="content">
                            <p class="bold  t-pad30">+389 71230015</p>
                            <p class="bold "><a href="#">Mrse_toni@hotmail.com</a></p>
                        </div>
                    </div>
                    <div class="contact">
                        <i class="icon-picons-building"></i>
                        <div class="content">
                            <p class="bold  t-pad30">Понеделник - Сабота: <br> 08:00 - 16:00
                            </p>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</footer>

