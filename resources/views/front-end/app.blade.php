<!DOCTYPE html>
<!--[if IE 8 ]>
<html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->

<head>
    <!-- Basic Page Needs
================================================== -->
    <meta charset="utf-8">
    <title>Mrse2007</title>
    <!-- Favicon
    ============================================== -->
    <!-- Mobile Specific
=================================================composer= -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- CSS Files
================================================== -->
@include('front-end.layouts.header')

<!-- Modernizr js file
================================================== -->
    <script src="{{asset('front-end/js/modernizr.custom.js')}}"></script>
</head>

<body>
<!-- loader -->
<div class="loader">
    <div class="spinner">
        <div class="double-bounce1"></div>
        <div class="double-bounce2"></div>
    </div>
</div>
<!-- /loader -->
<!-- content wrapper -->
<div class="content-wrap">

    <div class="content">

    @include('front-end.layouts.top-bar')

    @include('front-end.layouts.navigation')

    @include('front-end.layouts.msg')

    @yield('content')


        @include('front-end.layouts.footer')
<!--/content wrapper -->

<!-- JS Files
================================================== -->
@include('front-end.layouts.scripts')

</body>

</html>
